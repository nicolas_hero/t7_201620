package taller;



public class Pila <T>
{
	//------------------------ATRIBUTOS--------------------------
	
	//Nodo que hace referencia al tope de la pila.
	private Nodo tope; 
	
	//Cantidad de elementos almacenados en la pila.
	private int tam;
	
	class Nodo<T>
	
	{
		
		private T elemento;
		
		private Nodo siguiente;		
		
		
		Nodo(T pElemento)
		{
			elemento = pElemento;
			siguiente = null;
		}
		
		public T darElemento()
		{
			return elemento;
		}
		
		public Nodo darSiguiente()
		{
			return siguiente;
		}		
		
	}
	
	//------------------------CONSTRUCTOR------------------------
	
	public Pila()
	{
		tope = null;
		tam = 0;
	}
	
	//------------------------M�TODOS----------------------------
	
	public Nodo darTope()
	{
		return tope;
	}
	
	public int darTama�o()
	{
		return tam;
	}
	
	public boolean vacio()
	{
		boolean esVacio = false;
		if(tam == 0 && tope == null)
		{
			esVacio = true;
		}
		
		return esVacio;				
	}
	
	public void push(T pElemento)
	{
		Nodo nuevoTope = new Nodo(pElemento);
		
		if (vacio())
		{
			tope = nuevoTope;
		}
		else
		{
			nuevoTope.siguiente = tope;
			tope = nuevoTope;
		}	
		
		tam ++;
	}
	
	public T pop()
	{
		if (vacio())
		{
			return null;
		}
		else
		{
			T elementoTope = (T) tope.elemento;
			
			if(tope.siguiente != null)
			{
				tope = tope.siguiente;			
			}
			else
			{
				tope = null;
			}
			tam --;
			
			return elementoTope;
		}					
	}
}
