package taller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import org.json.simple.JSONObject;

import taller.interfaz.IReconstructorArbol;

public class Arbol implements IReconstructorArbol
{
	private Nodo raiz;
	private String preorden;
	private String inorden;
	private Pila<Nodo> stack;
	private  Lista listaInorden;
	private Lista listaPreorden;
	
	public Arbol()
	{
		raiz = null;
		stack = new Pila();
		listaInorden = new Lista();
		listaPreorden = new Lista();
	}
	public Nodo darRaizNodo()
	{
		return raiz;
	}
	public Lista darListaInOrden()
	{
		return listaInorden;
	}
	public Lista darListapreOrden()
	{
		return listaPreorden;
	}
	public boolean aLaIzquierda(String pV1, String pV2)
	{
		int a = listaInorden.posElemento(pV1);
		int b = listaInorden.posElemento(pV2);
		
		if(a<b )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void construirArbol()
	{
		int pre = 0;
		int last = listaPreorden.posElemento(listaPreorden.darUltimoEnLista());
		
		Nodo t = new Nodo(listaPreorden.get(pre));
		Nodo temp = t;
		
		int ac = pre;
		
		stack.push(temp);
		pre++;
		
		while(pre<= last)
		{
			if(aLaIzquierda(listaPreorden.get(pre),listaPreorden.get(ac) ))
			{
				temp.izq = new Nodo(listaPreorden.get(pre));
				temp = temp.izq;
				stack.push(temp);
				ac = pre;
				pre++;
			}
			else
			{
				Nodo prev = null;
				while(true)
				{
					temp = stack.pop();
					if(!aLaIzquierda(listaPreorden.get(pre), temp.darValor()))
					{
						prev = temp;
					}
					else
					{
						stack.push(temp);
						temp = prev;
						break;
					}
					if(stack.vacio())
					{
						break;
					}
				}
				temp.der = new Nodo(listaPreorden.get(pre));
				temp = temp.der;
				stack.push(temp);
				ac = pre;
				pre++;
			}
		}
		raiz = t;
	}
	
	public void preOrden()
	{
		preOrden(raiz);
	}
	
	public void preOrden(Nodo node)
	{
		if(node==null )
		{
			return;
		}
		
		System.out.print(node.darValor()+",");
		preOrden(node.izq);
		
		preOrden(node.der);
	}
	
	public void inOrden()
	{
		inOrden(raiz);
		System.out.println();
	}
	
	public void inOrden(Nodo node)
	{
		if(node==null )
		{
			return;
		}
		
		inOrden(node.izq);
		System.out.print(node.darValor()+",");
		inOrden(node.der);
	}
	
	private class Nodo
	{
		Nodo der;
		Nodo izq;
		
		String valor;
		
		public Nodo(String pVal)
		{
			this.valor = pVal;
		}
		
		public Nodo darDerecho()
		{
			return der;
		}
		public Nodo darIzquierdo()
		{
			return izq;
		}
		public String darValor()
		{
			return valor;
		}
	}

	public void cargarArchivo(String nombre) throws IOException 
	{
		Properties datos = new Properties();
		File archivo = new File(nombre);

		FileInputStream in = new FileInputStream(archivo);

		datos.load(in);
		in.close();
		String a = datos.getProperty("preorden");
		String h = datos.getProperty("inorden");
		preorden = a;
		inorden = h;
		reconstruir();
	}

	public void crearArchivo() throws FileNotFoundException,UnsupportedEncodingException
	{	
		JSONObject obj = new JSONObject();
		
		obj = createJSON(darRaizNodo());
		
		try
		{
			FileWriter file = new FileWriter("./data/plantado.json");
			file.write(obj.toJSONString());
			file.flush();
			file.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
        
	}
	
	public JSONObject createJSON(Nodo nodo)
	{
		if(nodo == null)
		{
			return null;
		}
		JSONObject nuevo = new JSONObject();
		nuevo.put("Valor", ""+nodo.darValor()+"");
		if(nodo.izq != null)
		{
			nuevo.put("Hijo izquierdo", createJSON(nodo.izq));
		}
		if(nodo.izq != null)
		{
			nuevo.put("Hijo derecho", createJSON(nodo.der));
		}
		System.out.println(nodo.darDerecho());
		return nuevo;
	}

	public void reconstruir()
	{
		String[] preorArr = preorden.split(",");
		String[] inArr = inorden.split(",");

		for (int i = 0; i < preorArr.length; i++)
		{
			darListapreOrden().agregar(preorArr[i]);
			darListaInOrden().agregar(inArr[i]);
		}

		construirArbol();
		
		preOrden();
		System.out.println();

		inOrden(); 
		
	}
	
	
}














